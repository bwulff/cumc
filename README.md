# CUDA Mission Control #

A dashboard displaying live information about NVIDIA GPUs on machines used by teams. 

## Requirements ##

- Python
- CUDA
- privileges to run 'nvidia-smi'